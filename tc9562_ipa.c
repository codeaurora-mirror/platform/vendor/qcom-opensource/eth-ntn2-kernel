/* Copyright (c) 2021, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "tc9562_ipa.h"
#include "dwmac4_dma.h"

#define NTN_IPA_PIPE_MAX_BW 800
#define NTN_IPA_PIPE_MIN_BW 0
#define NTN_IPA_DBG_MAX_MSG_LEN 3000
/*VLAN ids range for IPA offload*/
#define MIN_VLAN_ID 1
#define MAX_VLAN_ID 4094
#define GET_VALUE(data, lbit, hbit) ((data >>lbit) & (~(~0<<(hbit-lbit+1))))

static char buf[3000];

static void tc9562_ipaUcRdy_wq(struct work_struct *work);

/* Description of RX CH Status as per DMA_RXCHSTS register in spec*/
static const char *rx_ch_status_string[] = {
	"Stop State",
	"Descriptor Fetch State",
	"Wait For Status",
	"Writing Data to Memory",
	"Status Update",
	"Suspended State",
	"Closing Descriptor",
	"Reserved",
};

/* Description of TX CH Status as per DMA_TXCHSTS register in spec*/
static const char *tx_ch_status_string[] = {
	"Stop State",
	"Descriptor Fetch State",
	"Wait For Status",
	"Reading Data from Memory",
	"Status Update",
	"Suspended State",
	"Closing Descriptor",
	"Reserved",
};

/* Generic Bit descirption; reset = 0, set = 1*/
static const char *bit_status_string[] = {
	"Reset",
	"Set",
};

/* Generic Bit Mask Description; masked = 0, enabled = 1*/
static const char *bit_mask_string[] = {
	"Masked",
	"Enable",
};

#define IPA_ETH_RX_SOFTIRQ_THRESH 16

static int tc9562_yinit_offload(struct tc9562mac_priv *priv)
{
	int tx_ch = TC9562_TX_DMA_CH_0;
	int rx_ch = TC9562_RX_DMA_CH_0;

	//Clear Tx0/Rx0 DMA channel status reg
	DMA_CLRCHSTS_RgWr(0x3fffc7);
	//Enable interupts for Tx0/Rx0 DMA channel
	DMA_CHINTRMSK_RgWr(0xD041);

	/*** DMA Tx control config ***/
	DMA_TXCHCTL_OSP_UdfWr(tx_ch, 0x1);
	DMA_TXCHCTL_TXPBL_UdfWr(tx_ch, 16);

	/* To get Best Performance */
	DMA_BUSCFG_BLEN16_UdfWr(1);
	DMA_BUSCFG_BLEN8_UdfWr(1);
	DMA_BUSCFG_BLEN4_UdfWr(1);

	DMA_BUSCFG_RD_OSR_LMT_UdfWr(15);
	DMA_BUSCFG_WR_OSR_LMT_UdfWr(15);
	DMA_TXCHCTL_ST_UdfWr(tx_ch, 0x1);

	/*** DMA Rx control config ***/
	DMA_RXCHCTL_RBSZ_UdfWr(rx_ch, priv->dma_buf_sz);

	if ((priv->use_riwt) && (priv->hw->dma->rx_watchdog)) {
	    u32 rx_cnt = priv->plat->rx_queues_to_use + 1;
		priv->rx_riwt = MAX_DMA_RIWT;
		priv->hw->dma->rx_watchdog(priv->ioaddr, MAX_DMA_RIWT, rx_cnt);
	}
	/* set RX PBL = 128 bytes */
	DMA_RXCHCTL_RXPBL_UdfWr(rx_ch, 16);
	DMA_RXCHCTL_ST_UdfWr(rx_ch, 0x1);

	return 0;
}

int tc9562_enable_ipa_offload(struct tc9562mac_priv *pdata)
{
	int ret = 0;

	/* Enable offload flag in NTN HW */
	enable_offload(pdata);

	if (pdata->prv_ipa.ipa_uc_ready  && !pdata->prv_ipa.ipa_offload_init) {
		ret = tc9562_ipa_offload_init(pdata);
		if (ret) {
			pdata->prv_ipa.ipa_offload_init = false;
			NMSGPR_ERR("IPA Offload Init Failed \n");
			goto fail;
		}
		NDBGPR_L1("IPA Offload Initialized Successfully \n");
		pdata->prv_ipa.ipa_offload_init = true;
	}

	if (pdata->prv_ipa.ipa_uc_ready && !pdata->prv_ipa.ipa_offload_conn) {
		ret = tc9562_ipa_offload_connect(pdata);
		if (ret) {
			NMSGPR_ERR("IPA Offload Connect Failed \n");
			pdata->prv_ipa.ipa_offload_conn = false;
			goto fail;
		}
		NMSGPR_INFO("%s, IPA Offload Connect Successfully\n", __func__);
		pdata->prv_ipa.ipa_offload_conn = true;
	}

	/*Initialize DMA CHs for offload*/
	ret = tc9562_yinit_offload(pdata);
	if (ret) {
		NMSGPR_ERR("Offload channel Init Failed \n");
		goto fail;
	}

	if (!pdata->prv_ipa.ipa_debugfs_exists) {
		if (!tc9562_ipa_create_debugfs(pdata)) {
			NDBGPR_L1("NTN Debugfs created  \n");
			pdata->prv_ipa.ipa_debugfs_exists = true;
		} else NMSGPR_ERR("NTN Debugfs failed \n");
	}

	NMSGPR_INFO("IPA Offload Enabled successfully\n");
	return ret;

fail:
	if ( pdata->prv_ipa.ipa_offload_conn ) {
		if( tc9562_ipa_offload_disconnect(pdata) )
			NMSGPR_ERR("IPA Offload Disconnect Failed \n");
		else
			NDBGPR_L1("IPA Offload Disconnect Successfully \n");
		pdata->prv_ipa.ipa_offload_conn = false;
	}
	if ( pdata->prv_ipa.ipa_offload_init ) {
		if ( tc9562_ipa_offload_cleanup(pdata ))
			NMSGPR_ERR("IPA Offload Cleanup Failed \n");
		else
			NDBGPR_L1("IPA Offload Cleanup Success \n");
		pdata->prv_ipa.ipa_offload_init = false;
	}
	/* Disable offload flag in NTN HW */
	disable_offload(pdata);
	NMSGPR_INFO("%s failed \n", __func__);
	return ret;
}

int tc9562_disable_ipa_offload(struct tc9562mac_priv *pdata)
{
	int ret = 0;

	/* De-configure IPA Related Stuff */
	if (pdata->prv_ipa.ipa_offload_conn) {
		ret = tc9562_ipa_offload_suspend(pdata);
		if (ret) {
			NMSGPR_ERR("IPA Offload Disconnect Failed, err:%d\n", ret);
			return ret;
		}
		pdata->prv_ipa.ipa_offload_conn = false;
		NMSGPR_INFO("IPA Offload Disconnect Successfully \n");
	}

	if (pdata->prv_ipa.ipa_offload_init) {
		ret = tc9562_ipa_offload_cleanup(pdata);
		if (ret) {
			NMSGPR_ERR("IPA Offload Cleanup Failed, err: %d\n", ret);
			return ret;
		}
		NMSGPR_INFO("IPA Offload Cleanup Success \n");
		pdata->prv_ipa.ipa_offload_init = false;
	}
	NMSGPR_INFO("IPA Offload Disabled successfully\n");

	if (pdata->prv_ipa.ipa_debugfs_exists) {
		if (tc9562_ipa_cleanup_debugfs(pdata))
			NMSGPR_ERR("Unable to delete IPA debugfs\n");
		else
			pdata->prv_ipa.ipa_debugfs_exists = false;
	}

	/* Disable offload flag in NTN HW */
	disable_offload(pdata);

	return ret;
}


/*!
* \brief This sequence is used to initialize the rx descriptors.
* \param[in] priv
*/

static void rx_descriptor_init(struct tc9562mac_priv *priv, unsigned chInx)
{
	struct dma_desc *RX_NORMAL_DESC;
	int i;
	struct tc9562mac_rx_queue *rx_q = &priv->rx_queue[chInx];

	NDBGPR_L1("-->rx_descriptor_init\n");

	/* initialize all desc */
	for (i = 0; i < rx_q->dma_rx_desc_sz; i++) {
		if (priv->extend_desc)
				RX_NORMAL_DESC = &((rx_q->dma_erx + i)->basic);
			else
				RX_NORMAL_DESC = rx_q->dma_rx + i;

		memset(RX_NORMAL_DESC, 0, sizeof(struct dma_desc));

	/* update buffer 1 address pointer */
	if (priv->synopsys_id >= DWMAC_CORE_4_00) {
#ifdef TC9562_DEFINED
		RX_NORMAL_DESC->des0 = (rx_q->rx_skbuff_dma[i] & 0xFFFFFFFF);
		RX_NORMAL_DESC->des1 = HOST_PHYSICAL_ADRS_MASK | ((rx_q->rx_skbuff_dma[i] >> 32) & 0xF);
#else
		RX_NORMAL_DESC->des0 = cpu_to_le32(rx_q->rx_skbuff_dma[i]);
#endif
	}
	else
		RX_NORMAL_DESC->des2 = cpu_to_le32(rx_q->rx_skbuff_dma[i]);

		/* set buffer 2 address pointer to zero */
		RX_NORMAL_DESC->des2 = 0;
		/* set control bits - OWN, INTE and BUF1V */
		RX_NORMAL_DESC->des3 = 0xc1000000;

		/* Don't Set the IOC bit for IPA controlled Desc */
		if (priv->ipa_enabled && chInx == TC9562_RX_DMA_CH_0) {
			unsigned int varRDES3 = 0;
			varRDES3 = RX_NORMAL_DESC->des3;
			/* reset IOC for all buffers */
			RX_NORMAL_DESC->des3 = (varRDES3 & ~(1 << 30));
		}
	}

	/* Reset the OWN bit of last descriptor */
	if (priv->ipa_enabled && chInx == TC9562_RX_DMA_CH_0) {
		if (priv->extend_desc)
				RX_NORMAL_DESC = &((rx_q->dma_erx + rx_q->dma_rx_desc_sz- 1)->basic);
			else
				RX_NORMAL_DESC = rx_q->dma_rx + rx_q->dma_rx_desc_sz- 1;

		RX_NORMAL_DESC->des3 = 0;
	}

	/* update the total no of Rx descriptors count */
	priv->hw->dma->set_rx_ring_len(priv->ioaddr, (priv->rx_queue->dma_rx_desc_sz - 1), chInx);

	/* update the Rx Descriptor Tail Pointer */
	rx_q->rx_tail_addr = rx_q->dma_rx_phy +
			(rx_q->dma_rx_desc_sz * sizeof(struct dma_desc));
	priv->hw->dma->set_rx_tail_ptr(priv->ioaddr,
					   rx_q->rx_tail_addr,
					   chInx);

	/* update the starting address of desc chain/ring */
	priv->hw->dma->init_rx_chan(priv->ioaddr,
					priv->plat->dma_cfg,
					rx_q->dma_rx_phy, priv->dma_buf_sz, chInx);

	NDBGPR_L1("<--rx_descriptor_init\n");
}

/*!
* \brief This sequence is used to initialize the tx descriptors.
* \param[in] priv
*/

static void tx_descriptor_init(struct tc9562mac_priv *priv,
				unsigned chInx)
{
	struct dma_desc *TX_NORMAL_DESC;
	int i;
	struct tc9562mac_tx_queue *tx_q = &priv->tx_queue[chInx];

	NDBGPR_L1("-->tx_descriptor_init\n");

	/* initialze all descriptors. */

	for (i = 0; i < tx_q->dma_tx_desc_sz; i++) {
		TX_NORMAL_DESC = tx_q->dma_tx + i;
		if (priv->synopsys_id >= DWMAC_CORE_4_00) {
			/* update buffer 1 address pointer to zero */
			TX_NORMAL_DESC->des0 = 0;
			/* update buffer 2 address pointer to zero */
			TX_NORMAL_DESC->des1 = 0;
			/* set all other control bits (IC, TTSE, B2L & B1L) to zero */
			TX_NORMAL_DESC->des2 = 0;
			/* set all other control bits (OWN, CTXT, FD, LD, CPC, CIC etc) to zero */
			TX_NORMAL_DESC->des3 = 0;
		} else
			TX_NORMAL_DESC->des2 = 0;
	}

	/* update the total no of Tx descriptors count */
	priv->hw->dma->set_tx_ring_len(priv->ioaddr,
						       (priv->tx_queue->dma_tx_desc_sz - 1), chInx);
	/* update the starting address of desc chain/ring */
	priv->hw->dma->init_tx_chan(priv->ioaddr,
						    priv->plat->dma_cfg,
						    tx_q->dma_tx_phy, chInx);

	NDBGPR_L1("<--tx_descriptor_init\n");
}


/* This function is called from IOCTL when new RX/TX properties have to
   be registered with IPA e.g VLAN hdr insertion deletion */
int tc9562_disable_enable_ipa_offload(struct tc9562mac_priv *pdata, int chInx_tx_ipa, int chInx_rx_ipa)
{
	int ret = 0;

	pdata->hw->dma->stop_rx(pdata->ioaddr, chInx_rx_ipa);
        NDBGPR_L1("%s stop_dma_rx\n", __func__);

	pdata->hw->dma->stop_tx(pdata->ioaddr, chInx_tx_ipa);
	NDBGPR_L1("%s stop_dma_tx\n", __func__);

	/*disable IPA pipe first*/
	ret = tc9562_disable_ipa_offload(pdata);
	if ( ret ){
		NMSGPR_ERR("%s:%d unable to disable ipa offload\n",  __func__, __LINE__);
		return ret;
	}
	else {
		tx_descriptor_init(pdata, chInx_tx_ipa);
		rx_descriptor_init(pdata, chInx_rx_ipa);

		/*if VLAN-id is passed, then make the VLAN+ETH hdr
			and register the RX/TX properties*/

		ret = tc9562_enable_ipa_offload(pdata);
		if (ret) {
			NMSGPR_ERR("%s:%d unable to enable ipa offload\n", __func__, __LINE__);
			return ret;
		}
		else {
			pdata->hw->dma->start_tx(pdata->ioaddr, chInx_tx_ipa);
			NDBGPR_L1("%s start_dma_tx\n", __func__);
			pdata->hw->dma->start_rx(pdata->ioaddr, chInx_rx_ipa);
			NDBGPR_L1("%s start_dma_rx\n", __func__);
		}
	}

	return ret;
}

/**
 * tc9562_ipa_offload_suspend() - Suspend IPA offload data
 * path.
 *
 * IN: @priv: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_suspend(struct tc9562mac_priv *priv)
{
	int ret = 0;

	NDBGPR_L1("Suspend/disable IPA offload\n");

	priv->hw->dma->stop_rx(priv->ioaddr, TC9562_RX_DMA_CH_0);
	NDBGPR_L1("%s stop_dma_rx\n", __func__);

	/* Disconnect IPA offload */
	if (priv->prv_ipa.ipa_offload_conn) {
		ret = tc9562_ipa_offload_disconnect(priv);
		if (ret) {
			NMSGPR_ERR("IPA Offload Disconnect Failed, err:%d\n", ret);
			return ret;
		}
		priv->prv_ipa.ipa_offload_conn = false;
		NDBGPR_L1("IPA Offload Disconnect Successfully \n");
	}

	priv->hw->dma->stop_tx(priv->ioaddr, TC9562_TX_DMA_CH_0);
	NDBGPR_L1("%s stop_dma_tx\n", __func__);
	priv->prv_ipa.ipa_offload_susp = true;
	return ret;
}

/**
 * tc9562_ipa_offload_resume() - Resumes IPA offload data
 * path.
 *
 * IN: @pdata: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_resume(struct tc9562mac_priv *priv)
{
	int ret = 0;
	struct ipa_rm_perf_profile profile;

	NDBGPR_L1("%s <--\n", __func__);
	profile.max_supported_bandwidth_mbps = NTN_IPA_PIPE_MAX_BW;
	ret = ipa_rm_set_perf_profile(IPA_RM_RESOURCE_ODU_ADAPT_CONS,
					&profile);
	if (ret)
		NMSGPR_ERR("Err to set BW: IPA_RM_RESOURCE_ODU_ADAPT_CONS err:%d\n",
				ret);

	priv->prv_ipa.ipa_offload_susp = false;
	NDBGPR_L1("%s -->\n", __func__);
	return ret;
}
static int tc9562_ipa_uc_ready(struct tc9562mac_priv *priv)
{
	struct ipa_uc_ready_params param;
	int ret;

	param.is_uC_ready = false;
	param.priv = priv;
	param.notify = tc9562_ipa_uc_ready_cb;
	param.proto = IPA_UC_NTN;

	ret = ipa_uc_offload_reg_rdyCB(&param);
	if (ret == 0 && param.is_uC_ready) {
		NDBGPR_L1("%s:%d ipa uc ready\n", __func__, __LINE__);
		priv->prv_ipa.ipa_uc_ready = true;
	}

	return ret;
}

static void tc9562_ipa_ready_wq(struct work_struct *work)
{
	struct tc9562_prv_ipa_data *ntn_ipa = container_of(work,
				struct tc9562_prv_ipa_data, ntn_ipa_rdy_work);
	struct tc9562mac_priv *pdata = container_of(ntn_ipa,
					struct tc9562mac_priv, prv_ipa);
	int ret;

	pdata->prv_ipa.ipa_ready = true;
	ret = tc9562_ipa_offload_init(pdata);
	if (!ret) {
		NDBGPR_L1("IPA Offload Initialized Successfully \n");
		pdata->prv_ipa.ipa_offload_init = true;
	}

	ret = tc9562_ipa_uc_ready(pdata);
	if (ret == 0 && pdata->prv_ipa.ipa_uc_ready)
		tc9562_enable_ipa_offload(pdata);
}

static void tc9562_ipaUcRdy_wq(struct work_struct *work)
{
	struct tc9562_prv_ipa_data *ntn_ipa = container_of(work,
					struct tc9562_prv_ipa_data, ntn_ipa_rdy_work);
	struct tc9562mac_priv *pdata = container_of(ntn_ipa,
					struct tc9562mac_priv, prv_ipa);

	pdata->prv_ipa.ipa_uc_ready = true;
	tc9562_enable_ipa_offload(pdata);
}
/**
 * tc9562_ipa_ready_cb() - Callback register with IPA to
 * indicate if IPA is ready to receive configuration commands.
 * If IPA is not ready no IPA configuration commands should be
 * set.
 *
 * IN: @pdata: NTN private structure handle that will be passed by IPA.
 * OUT: NULL
 */
static void tc9562_ipa_ready_cb(void *user_data)
{
	struct tc9562mac_priv *pdata = (struct tc9562_prv_data *)user_data;
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;

	if(!pdata) {
		NMSGPR_ERR("%s Null Param pdata %p \n", __func__, pdata);
		return;
	}

	NDBGPR_L1("%s Received IPA ready callback\n",__func__);
	pdata->prv_ipa.ipa_ready = true;
	INIT_WORK(&ntn_ipa->ntn_ipa_rdy_work, tc9562_ipa_ready_wq);
	queue_work(system_unbound_wq, &ntn_ipa->ntn_ipa_rdy_work);
	return;
}

/**
 * tc9562_ipa_uc_ready_cb() - Callback register with IPA to indicate
 * if IPA (and IPA uC) is ready to receive configuration commands.
 * If IPA is not ready no IPA configuration commands should be set.
 *
 * IN: @pdata: NTN private structure handle that will be passed by IPA.
 * OUT: NULL
 */
void tc9562_ipa_uc_ready_cb(void *user_data)
{
	struct tc9562mac_priv *pdata = (struct tc9562_prv_data *)user_data;
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;

	if(!pdata) {
		NMSGPR_ERR("%s Null Param pdata %p \n", __func__, pdata);
		return;
	}

	NDBGPR_L1("%s Received IPA ready callback\n",__func__);
	pdata->prv_ipa.ipa_uc_ready = true;

	INIT_WORK(&ntn_ipa->ntn_ipa_rdy_work, tc9562_ipaUcRdy_wq);
	queue_work(system_unbound_wq, &ntn_ipa->ntn_ipa_rdy_work);

	return;
}

/**
 * ntn_ipa_notify_cb() - Callback registered with IPA to handle data packets.
 *
 * IN: @priv:             Priv data passed to IPA at the time of registration
 * IN: @ipa_dp_evt_type:  IPA event type
 * IN: @data:             Data to be handled for this event.
 * OUT: NULL
 */
static void ntn_ipa_notify_cb(void *priv, enum ipa_dp_evt_type evt,
				unsigned long data)
{
	struct tc9562mac_priv *pdata = (struct tc9562mac_priv *)priv;
	struct tc9562_prv_ipa_data *ntn_ipa;
	struct sk_buff *skb = (struct sk_buff *)data;
	struct iphdr *iph = NULL;

	if(!pdata || !skb) {
		NMSGPR_ERR("Null Param %s pdata %p skb %p \n", __func__, pdata, skb);
		return;
	}

	ntn_ipa = &pdata->prv_ipa;
	if(!ntn_ipa) {
		NMSGPR_ERR( "Null Param %s ntn_ipa %p \n", __func__, ntn_ipa);
		return;
	}

	NDBGPR_L2("%s %d EVT Rcvd %d \n", __func__, __LINE__, evt);

	if (!ntn_ipa->ipa_offload_conn) {
		NMSGPR_ERR("ipa_cb before offload is ready %s ipa_offload_conn %d  \n", __func__, ntn_ipa->ipa_offload_conn);
		return;
	}

	if (evt == IPA_RECEIVE) {
		/*Exception packets to network stack*/
		skb->dev = pdata->dev;
		skb_record_rx_queue(skb, TC9562_RX_DMA_CH_0);
		/*workaround solution in the driver to set the skb->protocol to Ipv4
		 if for L2TP tunnelled IPv4 pkt, IPA HW does not retain the L2 hdr
		 So if uCP processing has happened, set the protocl to IPv4 else
		 do normal EH frame processing. currently this issue only applies for
		 IPv4 */
		if ( true == *(u8 *)(skb->cb + 4) ){
			skb->protocol = htons( ETH_P_IP);
			iph = (struct iphdr *)skb->data;
		}
		else {
			skb->protocol = eth_type_trans(skb, skb->dev);
			iph = (struct iphdr *)(skb_mac_header(skb) + ETH_HLEN);
		}

		/* Submit packet to network stack */
		/* If its a ping packet submit it via rx_ni else use rx */
		if (iph->protocol == IPPROTO_ICMP) {
			netif_rx_ni(skb);
		}
		else if ((pdata->dev->stats.rx_packets %
				IPA_ETH_RX_SOFTIRQ_THRESH) == 0){
			netif_rx_ni(skb);
		} else {
			netif_rx(skb);
		}

		/* Update Statistics */
		pdata->ipa_stats.ipa_ul_exception++;
		pdata->dev->last_rx = jiffies;
		pdata->dev->stats.rx_packets++;
		pdata->dev->stats.rx_bytes += skb->len;
	} else {
		NMSGPR_ERR("Unhandled Evt %d ",evt);
		dev_kfree_skb_any(skb);
		pdata->dev->stats.tx_dropped++;
	}
}

/**
 * tc9562_ipa_offload_init() - Called from NTN driver to initialize IPA
 * offload data path.
 * This function will add partial headers and register the interface with IPA.
 *
 * IN: @pdata: NTN private structure handle that will be passed by IPA.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_init(struct tc9562mac_priv *pdata)
{
	struct ipa_uc_offload_intf_params in;
	struct ipa_uc_offload_out_params out;
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;
	struct ethhdr ntn_l2_hdr_v4;
	struct ethhdr ntn_l2_hdr_v6;
	struct vlan_ethhdr ntn_vlan_hdr_v4;
	struct vlan_ethhdr ntn_vlan_hdr_v6;
	int ret;

	if(!pdata) {
		NMSGPR_ERR("%s: Null Param\n", __func__);
		return -1;
	}

	memset(&in, 0, sizeof(in));
	memset(&out, 0, sizeof(out));

	/* Building ETH Header */
	if ( !pdata->prv_ipa.vlan_id ) {
		memset(&ntn_l2_hdr_v4, 0, sizeof(ntn_l2_hdr_v4));
		memset(&ntn_l2_hdr_v6, 0, sizeof(ntn_l2_hdr_v6));
		memcpy(&ntn_l2_hdr_v4.h_source, pdata->dev->dev_addr, ETH_ALEN);
		ntn_l2_hdr_v4.h_proto = htons(ETH_P_IP);
		memcpy(&ntn_l2_hdr_v6.h_source, pdata->dev->dev_addr, ETH_ALEN);
		ntn_l2_hdr_v6.h_proto = htons(ETH_P_IPV6);
		in.hdr_info[0].hdr = (u8 *)&ntn_l2_hdr_v4;
		in.hdr_info[0].hdr_len = ETH_HLEN;
		in.hdr_info[1].hdr = (u8 *)&ntn_l2_hdr_v6;
		in.hdr_info[1].hdr_len = ETH_HLEN;
	}
	if ( pdata->prv_ipa.vlan_id > MIN_VLAN_ID && pdata->prv_ipa.vlan_id <= MAX_VLAN_ID ) {
		memset(&ntn_vlan_hdr_v4, 0, sizeof(ntn_vlan_hdr_v4));
		memset(&ntn_vlan_hdr_v6, 0, sizeof(ntn_vlan_hdr_v6));
		memcpy(&ntn_vlan_hdr_v4.h_source, pdata->dev->dev_addr, ETH_ALEN);
		ntn_vlan_hdr_v4.h_vlan_proto = htons(ETH_P_8021Q);
		ntn_vlan_hdr_v4.h_vlan_encapsulated_proto = htons(ETH_P_IP);
		ntn_vlan_hdr_v4.h_vlan_TCI = htons(pdata->prv_ipa.vlan_id);
		in.hdr_info[0].hdr = (u8 *)&ntn_vlan_hdr_v4;
		in.hdr_info[0].hdr_len = VLAN_ETH_HLEN;
		memcpy(&ntn_vlan_hdr_v6.h_source, pdata->dev->dev_addr, ETH_ALEN);
		ntn_vlan_hdr_v6.h_vlan_proto = htons(ETH_P_8021Q);
		ntn_vlan_hdr_v6.h_vlan_encapsulated_proto = htons(ETH_P_IPV6);
		ntn_vlan_hdr_v6.h_vlan_TCI = htons(pdata->prv_ipa.vlan_id);
		in.hdr_info[1].hdr = (u8 *)&ntn_vlan_hdr_v6;
		in.hdr_info[1].hdr_len = VLAN_ETH_HLEN;
	}

	/* Building IN params */
	in.netdev_name = pdata->dev->name;
	in.priv = pdata;
	in.notify = ntn_ipa_notify_cb;
	in.proto = IPA_UC_NTN;
	in.hdr_info[0].dst_mac_addr_offset = 0;
	in.hdr_info[0].hdr_type = IPA_HDR_L2_ETHERNET_II;
	in.hdr_info[1].dst_mac_addr_offset = 0;
	in.hdr_info[1].hdr_type = IPA_HDR_L2_ETHERNET_II;

	ret = ipa_uc_offload_reg_intf(&in, &out);
	if (ret) {
		NMSGPR_ERR("Could not register offload interface ret %d \n",ret);
		return -1;
	}
	ntn_ipa->ipa_client_hndl = out.clnt_hndl;
	NDBGPR_L1("Recevied IPA Offload Client Handle %d",ntn_ipa->ipa_client_hndl);
	return 0;
}

/**
 * tc9562_ipa_offload_cleanup() - Called from NTN driver to cleanup IPA
 * offload data path.
 *
 * IN: @pdata: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_cleanup(struct tc9562mac_priv *pdata)
{
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;
	int ret = 0;

	if(!pdata) {
		NMSGPR_ERR( "Null Param %s \n", __func__);
		return -1;
	}

	if (!ntn_ipa->ipa_client_hndl) {
		NMSGPR_ERR("tc9562_ipa_offload_cleanup called with NULL"
				" IPA client handle \n");
		return -1;
	}

	ret = ipa_uc_offload_cleanup(ntn_ipa->ipa_client_hndl);
	if (ret) {
		NMSGPR_ERR("Could not cleanup IPA Offload ret %d\n",ret);
		return -1;
	}

	return 0;
}

/**
 * tc9562_ipa_offload_connect() - Called from NTN driver to connect IPA
 * offload data path. This function should be called from NTN driver after
 * allocation of rings and resources required for offload data path.
 *
 * After this function is called host driver should be ready to receive
 * any packets send by IPA.
 *
 * IN: @priv: NTN private structure handle that will be passed by IPA.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_connect(struct tc9562mac_priv *priv)
{
	struct tc9562_prv_ipa_data *ntn_ipa = &priv->prv_ipa;
	struct ipa_uc_offload_conn_in_params in;
	struct ipa_uc_offload_conn_out_params out;
	struct ipa_ntn_setup_info ul;
	struct ipa_ntn_setup_info dl;
	struct ipa_rm_perf_profile profile;
	int ret = 0;
	struct tc9562mac_tx_queue *tx_q = &priv->tx_queue[TC9562_TX_DMA_CH_0];
	struct tc9562mac_rx_queue *rx_q = &priv->rx_queue[TC9562_RX_DMA_CH_0];

	if(!priv) {
		NMSGPR_ERR( "Null Param %s \n", __func__);
		return -1;
	}

	memset(&in, 0, sizeof(in));
	memset(&out, 0, sizeof(out));
	memset(&profile, 0, sizeof(profile));

	in.clnt_hndl = ntn_ipa->ipa_client_hndl;
	/* Uplink Setup */
	ul.client = IPA_CLIENT_ODU_PROD;
	ul.ring_base_pa = (phys_addr_t)rx_q->dma_rx_phy;
	ul.ntn_ring_size = rx_q->dma_rx_desc_sz;
	ul.buff_pool_base_pa = virt_to_phys(GET_RX_BUFF_POOL_BASE_ADRR(TC9562_RX_DMA_CH_0));
	ul.num_buffers = rx_q->dma_rx_desc_sz - 1;
	ul.data_buff_size = TC9562_ETH_FRAME_LEN_IPA;
	ul.ntn_reg_base_ptr_pa = (phys_addr_t)TC9562_REG_PHY_BASE_ADRS;

	/* Downlink Setup */
	dl.client = IPA_CLIENT_ODU_TETH_CONS;
	dl.ring_base_pa = (phys_addr_t)tx_q->dma_tx_phy;
	dl.ntn_ring_size = tx_q->dma_tx_desc_sz;
	dl.buff_pool_base_pa = virt_to_phys(GET_TX_BUFF_DMA_POOL_BASE_ADRR(TC9562_TX_DMA_CH_0));
	dl.num_buffers = tx_q->dma_tx_desc_sz - 1;
	dl.data_buff_size = TC9562_ETH_FRAME_LEN_IPA;
	dl.ntn_reg_base_ptr_pa = (phys_addr_t)TC9562_REG_PHY_BASE_ADRS;

	/* Dump UL and DL Setups */
	NDBGPR_L1("IPA Offload UL client %d ring_base_pa %x ntn_ring_size %d buff_pool_base_pa %x num_buffers %d data_buff_size %d ntn_reg_base_ptr_pa %x ntn_reg_base_ptr %p \n",
		ul.client, ul.ring_base_pa, ul.ntn_ring_size, ul.buff_pool_base_pa, ul.num_buffers, ul.data_buff_size, ul.ntn_reg_base_ptr_pa, DMA_RXCH_DESC_TAILPTR_RgOffAddr(TC9562_RX_DMA_CH_0));
	NDBGPR_L1("IPA Offload DL client %d ring_base_pa %x ntn_ring_size %d buff_pool_base_pa %x num_buffers %d data_buff_size %d ntn_reg_base_ptr_pa %x ntn_reg_base_ptr %p \n",
		dl.client, dl.ring_base_pa, dl.ntn_ring_size, dl.buff_pool_base_pa, dl.num_buffers, dl.data_buff_size, dl.ntn_reg_base_ptr_pa, DMA_TXCH_DESC_TAILPTR_RgOffAddr(TC9562_TX_DMA_CH_0));

	in.u.ntn.ul = ul;
	in.u.ntn.dl = dl;

	ret = ipa_uc_offload_conn_pipes(&in, &out);
	if (ret) {
		NMSGPR_ERR("Could not connect IPA Offload Pipes %d\n", ret);
		return -1;
	}

    ntn_ipa->uc_db_rx_addr = out.u.ntn.ul_uc_db_pa;
    ntn_ipa->uc_db_tx_addr = out.u.ntn.dl_uc_db_pa;

	/* Set Perf Profile For PROD/CONS Pipes */
	profile.max_supported_bandwidth_mbps = NTN_IPA_PIPE_MAX_BW;
	ret = ipa_rm_set_perf_profile (IPA_RM_RESOURCE_ODU_ADAPT_PROD,
					&profile);
	if (ret) {
		NMSGPR_ERR("Err to set BW: IPA_RM_RESOURCE_ODU_ADAPT_PROD err:%d\n",
				ret);
		return -1;
	}
	ret = ipa_rm_set_perf_profile (IPA_RM_RESOURCE_ODU_ADAPT_CONS,
					&profile);
	if (ret) {
		NMSGPR_ERR("Err to set BW: IPA_RM_RESOURCE_ODU_ADAPT_CONS err:%d\n",
				ret);
		return -1;
	}
	__pm_stay_awake(&priv->prv_ipa.wlock);

	return 0;
}

/**
 * tc9562_ipa_offload_disconnect() - Called from NTN driver to disconnect IPA
 * offload data path. This function should be called from NTN driver before
 * de-allocation of any ring resources.
 *
 * After this function is successful, NTN is free to de-allocate the IPA controlled
 * DMA rings.
 *
 * IN: @priv: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_offload_disconnect(struct tc9562mac_priv *priv)
{
	struct tc9562_prv_ipa_data *ntn_ipa = &priv->prv_ipa;
	int ret = 0;

	if(!priv) {
		NMSGPR_ERR( "Null Param %s \n", __func__);
		return -1;
	}

	NDBGPR_L1("%s - begin \n",__func__);
	ret = ipa_uc_offload_disconn_pipes(ntn_ipa->ipa_client_hndl);
	if (ret) {
		NMSGPR_ERR("Could not cleanup IPA Offload ret %d\n",ret);
		return ret;
	}
	__pm_relax(&priv->prv_ipa.wlock);

	NDBGPR_L1("%s - end \n",__func__);
	return 0;
}

/*!
* \brief API to read FW version and features
* \param[in]
* \return register value
*/
static unsigned int read_fw_ver_features(struct tc9562mac_priv *pdata)
{
       return ntn_reg_rd( M3_SRAM_IPA_SET_DMEM_PCIE_ADDR,
                       PCIE_SRAM_BAR_NUM, pdata);
}

/**
 * read_ipa_stats() - Debugfs read command for IPA statistics
 *
 */
static ssize_t read_ipa_stats(struct file *file,
	char __user *user_buf, size_t count, loff_t *ppos)
{
	struct tc9562mac_priv *pdata = file->private_data;
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;
	char *buf;
	unsigned int len = 0, buf_len = 2000;
	ssize_t ret_cnt;

	if (!pdata || !ntn_ipa) {
		NMSGPR_ERR(" %s NULL Pointer \n",__func__);
		return -EINVAL;
	}

	buf = kzalloc(buf_len, GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	len += scnprintf(buf + len, buf_len - len, "\n \n");
	len += scnprintf(buf + len, buf_len - len, "%25s\n",
		"NTN IPA Stats");
	len += scnprintf(buf + len, buf_len - len, "%25s\n\n",
		"==================================================");

	len += scnprintf(buf + len, buf_len - len, "%-25s %10llu\n",
		"IPA RX Packets: ", pdata->ipa_stats.ipa_ul_exception);
	len += scnprintf(buf + len, buf_len - len, "\n");

	len += scnprintf(buf + len, buf_len - len, "%-25s %10d\n",
		"NTN FW Support IPA: ", ntn_fw_ipa_supported(pdata));
	len += scnprintf(buf + len, buf_len - len, "\n");

	len += scnprintf(buf + len, buf_len - len, "%-25s 0x%10x\n",
		"FW Version: ", read_fw_ver_features(pdata));
	len += scnprintf(buf + len, buf_len - len, "\n");

	if (len > buf_len)
		len = buf_len;

	ret_cnt = simple_read_from_buffer(user_buf, count, ppos, buf, len);
	kfree(buf);
	return ret_cnt;
}

void tc9562_ipa_stats_read(struct tc9562mac_priv *priv)
{
	struct tc9562_ipa_stats *dma_stats = &priv->ipa_stats;
	struct tc9562mac_tx_queue *tx_q = &priv->tx_queue[TC9562_TX_DMA_CH_0];
	struct tc9562mac_rx_queue *rx_q = &priv->rx_queue[TC9562_RX_DMA_CH_0];
	unsigned int data;

	dma_stats->ipa_rx_Desc_Ring_Base = rx_q->dma_rx_phy;
	dma_stats->ipa_rx_Desc_Ring_Size = rx_q->dma_rx_desc_sz;
	dma_stats->ipa_rx_Buff_Ring_Base =
		virt_to_phys(GET_RX_BUFF_POOL_BASE_ADRR(TC9562_RX_DMA_CH_0));
	dma_stats->ipa_rx_Buff_Ring_Size = rx_q->dma_rx_desc_sz - 1;
	dma_stats->ipa_rx_Db_Int_Raised =
	ntn_reg_rd(M3_DMA_RXCH0_DB_OFST, PCIE_SRAM_BAR_NUM, priv);
	DMA_RXCH_CUR_DESC_RgRd(TC9562_RX_DMA_CH_0, data);
	dma_stats->ipa_rx_Cur_Desc_Ptr_Indx = GET_RX_DESC_IDX(TC9562_RX_DMA_CH_0, data);
	DMA_RXCH_DESC_TAILPTR_RgRd(TC9562_RX_DMA_CH_0, data);
	dma_stats->ipa_rx_Tail_Ptr_Indx = GET_RX_DESC_IDX(TC9562_RX_DMA_CH_0, data);

	dma_stats->ipa_tx_Desc_Ring_Base = tx_q->dma_tx_phy;
	dma_stats->ipa_tx_Desc_Ring_Size = tx_q->dma_tx_desc_sz;
	dma_stats->ipa_tx_Buff_Ring_Base =
		 virt_to_phys(GET_TX_BUFF_DMA_POOL_BASE_ADRR(TC9562_TX_DMA_CH_0));
	dma_stats->ipa_tx_Buff_Ring_Size = tx_q->dma_tx_desc_sz - 1;
	dma_stats->ipa_tx_Db_Int_Raised =
	ntn_reg_rd(M3_DMA_TXCH0_DB_OFST, PCIE_SRAM_BAR_NUM, priv);
	DMA_TXCH_CUR_DESC_RgRd(TC9562_TX_DMA_CH_0, data);
	dma_stats->ipa_tx_Curr_Desc_Ptr_Indx = GET_TX_DESC_IDX(TC9562_TX_DMA_CH_0, data);
	DMA_TXCH_DESC_TAILPTR_RgRd(TC9562_TX_DMA_CH_0, data);
	dma_stats->ipa_tx_Tail_Ptr_Indx = GET_TX_DESC_IDX(TC9562_TX_DMA_CH_0, data);

	DMA_RXCHSTS_RgRd(TC9562_RX_DMA_CH_0, data);
	dma_stats->ipa_rx_tx_DMA_Status = data;
	dma_stats->ipa_rx_DMA_Err_Status =
		GET_VALUE(data, DMA_CHSTS_REB_LPOS, DMA_CHSTS_REB_HPOS);
	dma_stats->ipa_rx_DMA_Ch_underflow =
		GET_VALUE(data, DMA_CHSTS_RBU_LPOS, DMA_CHSTS_RBU_HPOS);
	dma_stats->ipa_rx_DMA_Transfer_stopped =
		GET_VALUE(data, DMA_CHSTS_RPS_LPOS, DMA_CHSTS_RPS_HPOS);
	dma_stats->ipa_rx_DMA_Transfer_complete =
		GET_VALUE(data, DMA_CHSTS_RI_LPOS, DMA_CHSTS_RI_HPOS);
	dma_stats->ipa_rx_DMA_Early_Trans_complete =
		GET_VALUE(data, DMA_CHSTS_ERI_LPOS, DMA_CHSTS_ERI_HPOS);
	dma_stats->ipa_tx_DMA_Err_Status =
		GET_VALUE(data, DMA_CHSTS_TEB_LPOS, DMA_CHSTS_TEB_HPOS);
	dma_stats->ipa_tx_DMA_Ch_underflow =
		GET_VALUE(data, DMA_CHSTS_TBU_LPOS, DMA_CHSTS_TBU_HPOS);
	dma_stats->ipa_tx_DMA_Transfer_stopped =
		GET_VALUE(data, DMA_CHSTS_TPS_LPOS, DMA_CHSTS_TPS_HPOS);
	dma_stats->ipa_tx_DMA_Transfer_complete =
		GET_VALUE(data, DMA_CHSTS_TI_LPOS, DMA_CHSTS_TI_HPOS);
	dma_stats->ipa_tx_DMA_Early_Trans_complete =
		GET_VALUE(data, DMA_CHSTS_ETI_LPOS, DMA_CHSTS_ETI_HPOS);

	DMA_RXCHINTMASK_RgRd(TC9562_RX_DMA_CH_0, dma_stats->ipa_rx_tx_Int_Mask);
	DMA_RXCHINTMASK_RCEN_UdfRd(TC9562_RX_DMA_CH_0,
				dma_stats->ipa_rx_Transfer_Complete_irq);
	DMA_RXCHINTMASK_RSEN_UdfRd(TC9562_RX_DMA_CH_0,
				dma_stats->ipa_rx_Transfer_Stopped_irq);
	DMA_RXCHINTMASK_UNFEN_UdfRd(TC9562_RX_DMA_CH_0, dma_stats->ipa_rx_Underflow_irq);
	DMA_RXCHINTMASK_ERIE_UdfRd(TC9562_RX_DMA_CH_0,
				dma_stats->ipa_rx_Early_Trans_Comp_irq);
	DMA_TXCHINTMASK_TCEN_UdfRd(TC9562_TX_DMA_CH_0,
				dma_stats->ipa_tx_Transfer_Complete_irq);
	DMA_TXCHINTMASK_TSEN_UdfRd(TC9562_TX_DMA_CH_0,
				dma_stats->ipa_tx_Transfer_Stopped_irq);
	DMA_TXCHINTMASK_UNFEN_UdfRd(TC9562_TX_DMA_CH_0,
				dma_stats->ipa_tx_Underflow_irq);
	DMA_TXCHINTMASK_ETCEN_UdfRd(TC9562_TX_DMA_CH_0,
				dma_stats->ipa_tx_Early_Trans_Comp_irq);
}

/**
 * read_ntn_dma_stats() - Debugfs read command for NTN DMA statistics
 * Only read DMA Stats for IPA Control Channels
 *
 */
static ssize_t read_ntn_dma_stats(struct file *file,
	char __user *user_buf, size_t count, loff_t *ppos)
{
	struct tc9562mac_priv *pdata = file->private_data;
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;
	struct tc9562_ipa_stats *dma_stats = &pdata->ipa_stats;
	char *buf;
	unsigned int len = 0, buf_len = 3000;
	ssize_t ret_cnt;

	buf = kzalloc(buf_len, GFP_KERNEL);
	if (!buf)
		return -ENOMEM;

	tc9562_ipa_stats_read(pdata);

	len += scnprintf(buf + len, buf_len - len, "\n \n");
	len += scnprintf(buf + len, buf_len - len, "%25s\n",
		"NTN DMA Stats");
	len += scnprintf(buf + len, buf_len - len, "%25s\n\n",
		"==================================================");

	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"RX Desc Ring Base: ", dma_stats->ipa_rx_Desc_Ring_Base);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"RX Desc Ring Size: ", dma_stats->ipa_rx_Desc_Ring_Size);
	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"RX Buff Ring Base: ", dma_stats->ipa_rx_Buff_Ring_Base);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"RX Buff Ring Size: ", dma_stats->ipa_rx_Buff_Ring_Size);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10u\n",
		"RX Doorbell Interrupts Raised: ", dma_stats->ipa_rx_Db_Int_Raised);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"RX Current Desc Pointer Index: ", dma_stats->ipa_rx_Cur_Desc_Ptr_Indx);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"RX Tail Pointer Index: ", dma_stats->ipa_rx_Tail_Ptr_Indx);
	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"RX Doorbell Address: ", ntn_ipa->uc_db_rx_addr);
	len += scnprintf(buf + len, buf_len - len, "\n");

	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"TX Desc Ring Base: ", dma_stats->ipa_tx_Desc_Ring_Base);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"TX Desc Ring Size: ", dma_stats->ipa_tx_Desc_Ring_Size);
	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"TX Buff Ring Base: ", dma_stats->ipa_tx_Buff_Ring_Base);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10d\n",
		"TX Buff Ring Size: ", dma_stats->ipa_tx_Buff_Ring_Size);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10u\n",
		"TX Doorbell Interrupts Raised: ", dma_stats->ipa_tx_Db_Int_Raised);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10lu\n",
		"TX Current Desc Pointer Index: ", dma_stats->ipa_tx_Curr_Desc_Ptr_Indx);
	len += scnprintf(buf + len, buf_len - len, "%-50s %10lu\n",
		"TX Tail Pointer Index: ", dma_stats->ipa_tx_Tail_Ptr_Indx);
	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"TX Doorbell Address: ", ntn_ipa->uc_db_tx_addr);
	len += scnprintf(buf + len, buf_len - len, "\n");

	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"RX TX DMA Status: ", dma_stats->ipa_rx_tx_DMA_Status);
	len += scnprintf(buf + len, buf_len - len, "\n");

	len += scnprintf(buf + len, buf_len - len, "%-50s 0x%x\n",
		"RX TX DMA CH0 INT Mask: ", dma_stats->ipa_rx_tx_Int_Mask);

	if (len > buf_len)
		len = buf_len;

	ret_cnt = simple_read_from_buffer(user_buf, count, ppos, buf, len);
	kfree(buf);
	return ret_cnt;
}

static ssize_t read_ipa_offload_status(struct file *file,
	char __user *user_buf, size_t count, loff_t *ppos)
{
	unsigned int len = 0, buf_len = NTN_IPA_DBG_MAX_MSG_LEN;
	struct tc9562mac_priv *pdata = file->private_data;

	if (pdata->prv_ipa.ipa_offload_susp)
		len += scnprintf(buf + len, buf_len - len, "IPA Offload suspended\n");
	else
		len += scnprintf(buf + len, buf_len - len, "IPA Offload enabled\n");

	if (len > buf_len)
		len = buf_len;

	return simple_read_from_buffer(user_buf, count, ppos, buf, len);
}

static ssize_t suspend_resume_ipa_offload(struct file *file,
	char __user *user_buf, size_t count, loff_t *ppos)
{
	s8 option = 0;
	char in_buf[2];
	unsigned long ret;
	struct tc9562mac_priv *pdata = file->private_data;

	if (sizeof(in_buf) < 2)
		return -EFAULT;

	ret = copy_from_user(in_buf, user_buf, 1);
	if (ret)
		return -EFAULT;

	in_buf[1] = '\0';
	if (kstrtos8(in_buf, 0, &option))
		return -EFAULT;

	if (option == 1 && !pdata->prv_ipa.ipa_offload_susp)
		tc9562_ipa_offload_suspend(pdata);
	else if (option == 0 && pdata->prv_ipa.ipa_offload_susp)
		tc9562_ipa_offload_resume(pdata);

	return count;
}


static const struct file_operations fops_ipa_stats = {
	.read = read_ipa_stats,
	.open = simple_open,
	.owner = THIS_MODULE,
	.llseek = default_llseek,
};

static const struct file_operations fops_ntn_dma_stats = {
	.read = read_ntn_dma_stats,
	.open = simple_open,
	.owner = THIS_MODULE,
	.llseek = default_llseek,
};

static const struct file_operations fops_ntn_ipa_offload_en = {
	.read = read_ipa_offload_status,
	.write = suspend_resume_ipa_offload,
	.open = simple_open,
	.owner = THIS_MODULE,
	.llseek = default_llseek,
};
/**
 * tc9562_ipa_create_debugfs() - Called from NTN driver to create debugfs node
 * for offload data path debugging.
 *
 * IN: @pdata: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_create_debugfs(struct tc9562mac_priv *pdata)
{
	struct tc9562_prv_ipa_data *ntn_ipa = &pdata->prv_ipa;
	static struct dentry *stats = NULL;

	if(!pdata) {
		NMSGPR_ERR( "Null Param %s \n", __func__);
		return -1;
	}

	ntn_ipa->debugfs_dir = debugfs_create_dir("eth", NULL);
	if (!ntn_ipa->debugfs_dir) {
		NMSGPR_ERR( "Cannot create debugfs dir %d \n", (int)ntn_ipa->debugfs_dir);
		return -ENOMEM;
	}

	stats = debugfs_create_file("ipa_stats", S_IRUSR, ntn_ipa->debugfs_dir,
				pdata, &fops_ipa_stats);
	if (!stats || IS_ERR(stats)) {
		NMSGPR_ERR( "Cannot create debugfs ipa_stats %d \n", (int)stats);
		goto fail;
	}

	stats = debugfs_create_file("dma_stats", S_IRUSR, ntn_ipa->debugfs_dir,
				pdata, &fops_ntn_dma_stats);
	if (!stats || IS_ERR(stats)) {
		NMSGPR_ERR( "Cannot create debugfs dma_stats %d \n", (int)stats);
		goto fail;
	}

	stats = debugfs_create_file("suspend_ipa_offload", (S_IRUSR|S_IWUSR),
				ntn_ipa->debugfs_dir, pdata, &fops_ntn_ipa_offload_en);
	if (!stats || IS_ERR(stats)) {
		NMSGPR_ERR( "Cannot create debugfs ipa_offload_en %d \n", (int)stats);
		goto fail;
	}
	return 0;

fail:
	debugfs_remove_recursive(ntn_ipa->debugfs_dir);
	return -ENOMEM;
}

/**
 * tc9562_ipa_cleanup_debugfs() - Called from NTN driver to cleanup debugfs node
 * for offload data path debugging.
 *
 * IN: @pdata: NTN dirver private structure.
 * OUT: 0 on success and -1 on failure
 */
int tc9562_ipa_cleanup_debugfs(struct tc9562mac_priv *pdata)
{
	struct tc9562_prv_ipa_data *ntn_ipa;

	if(!pdata) {
		NMSGPR_ERR("Null Param %s \n", __func__);
		return -1;
	}

	ntn_ipa = &pdata->prv_ipa;
	if (ntn_ipa->debugfs_dir)
		debugfs_remove_recursive(ntn_ipa->debugfs_dir);

	NDBGPR_L1("IPA debugfs Deleted Successfully \n");
	return 0;
}
int tc9562_ipa_ready(struct tc9562mac_priv *pdata)
{
	int ret = 0;

	if (pdata->prv_ipa.ipa_ver >= IPA_HW_v3_0) {
		ret = ipa_register_ipa_ready_cb(tc9562_ipa_ready_cb,
										(void *)pdata);
		if (ret == -ENXIO) {
			NMSGPR_ERR("%s: IPA driver context is not even ready\n", __func__);
			pdata->prv_ipa.ipa_ready = false;
			return ret;
		}

		if (ret != -EEXIST) {
			NDBGPR_L1("%s:%d register ipa ready cb\n", __func__, __LINE__);
			return ret;
		}
	}

	pdata->prv_ipa.ipa_ready = true;
	NDBGPR_L1("%s:%d ipa ready\n", __func__, __LINE__);
	ret = tc9562_ipa_uc_ready(pdata);

	return ret;
}
